import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Politic {
    public int  sou;
    public String nom,nif;
    public boolean corrupt;
    public LocalDate datenaix;

    public Politic(String nif, int sou, String nom, boolean corrupt, LocalDate datenaix) {
        this.nif = nif;
        this.sou = sou;
        this.nom = nom;
        this.corrupt = corrupt;
        this.datenaix = datenaix;
    }


    public Politic() {

    }

    public int getSou() {
        return sou;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public boolean isCorrupt() {
        return corrupt;
    }

    public void setCorrupt(boolean corrupt) {
        this.corrupt = corrupt;
    }

    public LocalDate getDatenaix() {
        return datenaix;
    }

    public void setDatenaix(LocalDate datenaix) {
        this.datenaix = datenaix;
    }

    @Override
    public String toString () {
        String sortida = "NIF: "+ this.nif+"\n";
        sortida += "nom: "+ this.nom + "\n";
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        sortida += "data naixement: " + this.datenaix.format(DateTimeFormatter.ofPattern("d/MM/yyyy")) +"\n";
        sortida += "sou: "+this.sou+"\n";
        sortida += "és corrupte?: " + ((this.corrupt) ? "Sí" : "No") +"\n";
        return sortida;
    }

}
