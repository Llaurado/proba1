import javax.swing.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Jbdc {
        private static Connection myconn;

    public static int menu() {
        System.out.println("Politics:\n1.Crear taula\n2.Insertar politic\n3.Llegir taula\n0.Sortir");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();

    }

    public static void crear() {
        try {
             myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/basededatos", "usuario", "jupiter");
            Statement mystmn = myconn.createStatement();

            String borrar = "DROP table politic";
            mystmn.execute(borrar);

            String crear = "CREATE TABLE politic (nif VARCHAR(10) PRIMARY KEY,nom VARCHAR(100) NOT NULL,dataNaixement DATE,sou INT,esCorrupte ENUM('S','N'))";
            mystmn.execute(crear);

            System.out.println("taula creada");

            myconn.close();


        } catch (Exception e) {

            e.printStackTrace();
        }
    }
    public static Politic insereix(Politic p) throws SQLException {
        myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/databasem06", "jupiter", "jupiter");
        String sentenciaSQL = "insert into politic (nif,nom,dataNaixement,sou,esCorrupte) values (?,?,?,?,?)";

        PreparedStatement sentenciaPreparada = myconn.prepareStatement(sentenciaSQL);
        sentenciaPreparada.setString(1, String.valueOf(p.nif));
        sentenciaPreparada.setString(2,p.nom);
        sentenciaPreparada.setDate(3,java.sql.Date.valueOf(p.datenaix));
        sentenciaPreparada.setInt(4,p.sou);
        sentenciaPreparada.setString(5, ((p.corrupt) ? "S" : "N"));
        sentenciaPreparada.executeUpdate();
        System.out.println("Polític inserit correctament.");
        Politic p1=p;
        return p1;

    }

    public static List<Politic> llegeix() throws SQLException {
        ArrayList<Politic> llista = new ArrayList<>();
        myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/databasem06", "jupiter", "jupiter");
        Statement mystmn = myconn.createStatement();
        String select="select * from politic";
        mystmn = myconn.createStatement();
        ResultSet result=mystmn.executeQuery(select);

        while (result.next()) {
            Politic p1 =new Politic();
            p1.nif = result.getString("nif");
            p1.nom = result.getString("nom");
            p1.datenaix =  result.getDate("dataNaixement").toLocalDate();
            p1.corrupt = result.getString("esCorrupte").equals("S");
            p1.sou = result.getInt("sou");
            llista.add(p1);

        }
        return llista;



    }


    public static void main(String[] args)throws SQLException {
        Politic p = null;
        int opcio = menu();
        while (opcio != 0) {
            switch (opcio) {
                case 1:
                    crear();
                    break;
                 case 2:
                     System.out.println("Introdueix nif politic");
                     Scanner sc=new Scanner(System.in);
                     String nif=sc.nextLine();
                     System.out.println("Introdueix nom politic");
                     String nom=sc.nextLine();
                     System.out.println("Introdueix datanaix politic(dd/MM/yyyy)");
                     String data = sc.nextLine();
                     LocalDate date = LocalDate.parse(data,DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                     System.out.println("Introdueix si el politic es corrupte(true/false)");
                     boolean corrupt= sc.nextBoolean();
                     System.out.println("Introdueix sou politic");
                     int sou= sc.nextInt();

                    p = new Politic(nif,sou,nom,corrupt,date);
                    System.out.println(p);
                    insereix(p);
                    break;
                case 3:
                    List<Politic> llistaPolitics =llegeix();
                    System.out.println("=== LLISTA DE POLÍTICS ====");
                    for (Politic pi : llistaPolitics) {
                        System.out.println(pi);
                    }
                    System.out.println("===========================");
                    break;
            }

            opcio = menu();
        }
            }
        }






